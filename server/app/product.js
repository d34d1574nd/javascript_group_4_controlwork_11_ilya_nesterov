const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');


const auth = require('../middleware/auth');
const Product = require('../models/Product');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();


router.post('/', auth, upload.single('image'), (req, res) => {
  const productData = {...req.body, user: req.user._id};

  if (req.file) {
    productData.image = req.file.filename;
  }

  if (req.body.price < 0) {
    return res.sendStatus(403)
  }

  const product = new Product(productData);

    product.save()
      .then(result => res.send(result))
      .catch(error => res.send(error))
});

router.get('/', (req, res) => {
  if (req.query.category) {
    Product.find({category: req.query.category}).populate('user').populate('category', '_id, title')
      .then(result => res.send(result))
      .catch(error => res.send('Bad request', error))
  } else {
    Product.find().populate('user').populate('category', '_id, title')
      .then(result => res.send(result))
      .catch(error => res.send('Bad request', error))
  }
});

router.get('/:id', (req, res) => {
  Product.findById(req.params.id).populate('user').populate('category', 'title')
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error))
});

router.delete('/:id', auth, (req, res) => {
  Product.findByIdAndDelete({_id: req.params.id, user: req.user._id})
    .then(result => res.send(result))
});

module.exports = router;