const express = require('express');

const Category = require('../models/Category');

const router = express.Router();

router.get('/', (req, res) => {
  Category.find()
    .then(result => res.send(result))
    .catch(error => res.status(400).send('Bad request', error))
});

module.exports = router;