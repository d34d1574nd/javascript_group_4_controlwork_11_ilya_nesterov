const mongoose = require('mongoose');
const nanoid = require('nanoid');
const config = require('./config');

const User = require('./models/User');
const Category = require('./models/Category');
const Product = require('./models/Product');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const users = await User.create(
    {username: 'Ilya', password: '123', displayName: 'D34D', phone: 996555555555, token: nanoid()},
    {username: 'John', password: '123', displayName: 'John', phone: 996555555555, token: nanoid()}
  );

  const category = await Category.create(
    {title: 'Computers'},
    {title: 'Game station'},
    {title: 'School item'},
    {title: 'Accessories'}
  );

  await Product.create(
    {user: users[0]._id, title: 'PC', description: 'Very cool pc', image: 'comp.jpeg', category: category[0]._id, price: 25000},
    {user: users[1]._id, title: 'PlayStation 4', description: 'playstation 4', image: 'ps4.jpeg', category: category[1]._id, price: 19500},
    {user: users[1]._id, title: 'Algebra', description: 'school book for 11 class', image: 'books.jpeg', category: category[2]._id, price: 150},
  );

  return connection.close();
};


run().catch(error => {
  console.error('Something wrong happened...', error);
});
