import React, {Component, Fragment} from 'react';
import {deleteProduct, fetchOneProduct} from "../../store/actions/productAction";
import {connect} from "react-redux";
import {Button, Card, CardBody, CardFooter, CardHeader, CardText, CardTitle, Col, Row} from "reactstrap";
import Thumbnail from "../../components/Thumbnail/Thumbnail";
import Spinner from "../../components/UI/Spinner/Spinner";
import {Redirect} from "react-router-dom";

import './OneProducet.css'

class OneProduct extends Component {
  componentDidMount() {
    this.props.fetchOneProduct(this.props.match.params.id)
  }

  render() {
    if (!this.props.user) return <Redirect to='/' />;
    if (!this.props.product) return <Spinner />;
    return (
      <Fragment>
        <Button className='button' onClick={this.props.history.goBack}>Go back</Button>
        <Card>
          <CardHeader>
            <Row>
              <Col sm="4">
                <strong>Category: </strong>
                {this.props.product.category.title}
              </Col>
              <Col sm="4">
                <strong>Product title: </strong>
                {this.props.product.title}
              </Col>
            </Row>
          </CardHeader>
          <CardBody>
            <CardTitle>
              <Row>
                <Col sm="4">
                  <strong>Name: </strong>
                  {this.props.product.user.displayName}
                </Col>
                <Col sm="4">
                  <strong>Phone number: </strong>
                  +{this.props.product.user.phone}
                </Col>
              </Row>
            </CardTitle>
            <Thumbnail image={this.props.product.image}/>
            <hr/>
            <strong>Description:</strong>
            <CardText>{this.props.product.description}</CardText>
          </CardBody>
          <CardFooter>
            <Row>
              <Col>
                <strong>Price: </strong>
                {this.props.product.price}
                <strong> KGS</strong>
              </Col>
              <Col>
                {this.props.user._id === this.props.product.user._id ? <Button onClick={() => this.props.deleteProduct(this.props.match.params.id)}>Продано</Button> : null}
              </Col>
            </Row>

          </CardFooter>
        </Card>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  product: state.products.product,
  spinner: state.products.spinner,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  fetchOneProduct: idProduct => dispatch(fetchOneProduct(idProduct)),
  deleteProduct: idProduct => dispatch(deleteProduct(idProduct))
});

export default connect(mapStateToProps, mapDispatchToProps)(OneProduct);