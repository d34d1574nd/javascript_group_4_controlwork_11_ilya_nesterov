import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {postProduct} from "../../store/actions/productAction";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {fetchCategory} from "../../store/actions/categoryActions";

import './NewProduct.css';
import {Redirect} from "react-router-dom";

class NewProduct extends Component {
  state = {
    title: '',
    description: '',
    image: null,
    category: '',
    price: ''
  };

  componentDidMount() {
    this.props.fetchCategory()
  }

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onProductCreated(formData);

  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    })
  };

  render() {
    if (!this.props.user) return <Redirect to='/' />;
    return (
      <Fragment>
        <Form onSubmit={this.submitFormHandler}>
          <FormGroup row>
            <Label sm={2} for="category">Category</Label>
            <Col sm={10}>
              <Input
                type="select" required
                name="category" id="category"
                value={this.state.category}
                onChange={this.inputChangeHandler}
              >
                <option value="">Please select category...</option>
                {this.props.category.map(category => (
                  <option key={category._id} value={category._id}>{category.title}</option>
                ))}
              </Input>
            </Col>
          </FormGroup>
          <FormElement
            propertyName="title" title="Title"
            type="text" value={this.state.title}
            onChange={this.inputChangeHandler}
            placeholder="Enter a title of your product"
            required={true}
          />
          <FormElement
            propertyName="description" title="Description"
            type="textarea" value={this.state.description}
            onChange={this.inputChangeHandler}
            placeholder="Enter product description"
            required={true}
          />
          <FormElement
            propertyName="price" title="Price"
            type="number" value={this.state.price}
            onChange={this.inputChangeHandler}
            required={true}
          />
          <FormElement
            propertyName="image" title="Image" required={true}
            type="file" onChange={this.fileChangeHandler}
          />
          <Button type="submit">Add product</Button>
        </Form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  category: state.category.category,
  user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
  fetchCategory: () => dispatch(fetchCategory()),
  onProductCreated: productData => dispatch(postProduct(productData))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewProduct);