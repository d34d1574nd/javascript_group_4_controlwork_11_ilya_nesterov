import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Card, CardBody, CardFooter, CardHeader, Col, NavLink, Row} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

import Category from "../../components/Category/Category";
import {fetchCategoryProduct, fetchProducts} from "../../store/actions/productAction";
import Thumbnail from "../../components/Thumbnail/Thumbnail";
import Spinner from "../../components/UI/Spinner/Spinner";

import "./Products.css";


class Product extends Component {

  componentDidMount() {
    this.props.fetchProducts();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.categoryId !== prevProps.match.params.categoryId) {
      this.props.fetchCategoryProduct(this.props.match.params.categoryId)
    }
    if (!this.props.match.params.categoryId) {
      this.props.fetchProducts();
    }
  }

  render() {
    if (this.props.spinner) {
      return <Spinner />;
    }
    const product = this.props.products.map(product => {
      return (
        <Col sm="3" key={product._id}>
        <Card >
          <CardHeader><strong>Title: </strong>{product.title}</CardHeader>
          <CardBody>
            <Thumbnail image={product.image}/>
          </CardBody>
          <CardFooter>
            <NavLink tag={RouterNavLink} to={'/product/' + product._id}>{product.price} <strong>KGS</strong></NavLink>
          </CardFooter>
        </Card>
        </Col>
      )
    });
    return (
      <Fragment>
        <div className='category'>
          <Category />
        </div>
        <div className='products'>
          <Row>
            {product}
          </Row>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products.products,
  spinner: state.products.spinner
});

const mapDispatchToProps = dispatch => ({
  fetchProducts: () => dispatch(fetchProducts()),
  fetchCategoryProduct: idCategory => dispatch(fetchCategoryProduct(idCategory))
});

export default connect(mapStateToProps, mapDispatchToProps)(Product);