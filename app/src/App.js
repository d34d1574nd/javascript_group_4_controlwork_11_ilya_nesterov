import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {NotificationContainer} from 'react-notifications';

import Toolbar from "./components/UI/Toolbar/Toolbar";
import Register from "./containers/Register/Register";
import Login from './containers/Login/Login';
import {logoutUsers} from "./store/actions/usersActions";
import Product from "./containers/Product/Product";
import NewProduct from "./containers/NewProduct/NewProduct";
import OneProduct from "./containers/OneProduct/OneProduct";

class App extends Component {
  render() {
    return (
      <Fragment>
        <header>
          <NotificationContainer/>
          <Toolbar
            user={this.props.user}
            logout={this.props.logoutUser}
          />
        </header>
        <Container style={{marginTop: '20px'}}>
          <Switch>
            <Route path="/" exact component={Product}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/new_product" exact component={NewProduct}/>
            <Route path="/product/:id" exact component={OneProduct}/>
            <Route path="/category/:categoryId" exact component={Product}/>
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUsers())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
