import axios from '../../axios-api';
import {push} from "connected-react-router";
import {NotificationManager} from "react-notifications";

export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const FETCH_ONE_PRODUCT_SUCCESS = 'FETCH_ONE_PRODUCT_SUCCESS';
export const LOADING_PRODUCTS = 'LOADING_PRODUCTS';

const fetchProductSuccess = products => ({type: FETCH_PRODUCT_SUCCESS, products});
const fetchOneProductSuccess = product => ({type: FETCH_ONE_PRODUCT_SUCCESS, product});
const loadingProducts = cancel => ({type: LOADING_PRODUCTS, cancel});



export const fetchProducts = () => {
  return dispatch => {
    axios.get('/products')
      .then(response => dispatch(fetchProductSuccess(response.data)))
      .finally(() => dispatch(loadingProducts(false)))
  }
};

export const fetchCategoryProduct = idCategory => {
  return dispatch => {
    axios.get('/products?category=' + idCategory)
      .then(response => dispatch(fetchProductSuccess(response.data)))
      .finally(() => dispatch(loadingProducts(false)))
  }
};

export const fetchOneProduct = idProduct => {
  return dispatch => {
    axios.get('/products/' + idProduct).then(response => dispatch(fetchOneProductSuccess(response.data)))
  }
};

export const postProduct = productData => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const config = {headers: {'Authorization': token}};

    axios.post('/products', productData, config).then(() => {
      dispatch(fetchProducts());
      NotificationManager.success('Product created!');
      dispatch(push('/'));
    }).catch(() => {
      NotificationManager.error('Price < 0');
    })
  }
};

export const deleteProduct = idProduct => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const config = {headers: {'Authorization': token}};
    axios.delete('/products/' + idProduct, config).then(() => {
      NotificationManager.success('Product is deleted!');
      dispatch(push('/'));
    })
  }
};