import axios from '../../axios-api';

export const FETCH_CATEGORY_SUCCESS = 'FETCH_CATEGORY_SUCCESS';

const fetchCategorySuccess = category => ({type: FETCH_CATEGORY_SUCCESS, category});

export const fetchCategory = () => {
  return dispatch => {
    axios.get('/category').then(response => dispatch(fetchCategorySuccess(response.data)))
  }
};