import {
  FETCH_ONE_PRODUCT_SUCCESS,
  FETCH_PRODUCT_SUCCESS,
  LOADING_PRODUCTS
} from "../actions/productAction";

const initialState = {
  products: [],
  product: null,
  spinner: true
};

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCT_SUCCESS:
      return {...state, products: action.products};
    case FETCH_ONE_PRODUCT_SUCCESS:
      return {...state, product: action.product};
    case LOADING_PRODUCTS:
      return {...state, spinner: action.cancel};
    default:
      return state;
  }
};

export default productReducer;