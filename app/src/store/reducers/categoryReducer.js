import {FETCH_CATEGORY_SUCCESS} from "../actions/categoryActions";

const initialState = {
  category: []
};

const categoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CATEGORY_SUCCESS:
      return {...state, category: action.category};
    default:
      return state;
  }
};

export default categoryReducer;