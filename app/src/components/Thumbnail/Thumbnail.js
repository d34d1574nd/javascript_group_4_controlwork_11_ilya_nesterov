import React from 'react';

import {apiURL} from "../../constants";

const styles = {
  width: '100px',
  height: '100px',
  marginRight: '10px'
};

const Thumbnail = props => {
  let image = null;

  if (props.image) {
    image = apiURL + '/uploads/' + props.image;
  }

  return <img src={image} style={styles} className="img-thumbnail" alt="lalafu" />;
};

export default Thumbnail;
