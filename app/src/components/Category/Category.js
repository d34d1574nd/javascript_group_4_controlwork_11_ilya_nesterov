import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {NavLink as RouterNavLink} from "react-router-dom";
import {NavLink} from "reactstrap";

import {fetchCategory} from "../../store/actions/categoryActions";


class Category extends Component {
  componentDidMount() {
    this.props.fetchCategory()
  }

  render() {
    const category = this.props.category.map(category => {
      return (
        <NavLink tag={RouterNavLink} to={'/category/' + category._id}  key={category._id}>
          <li>
            {category.title}
          </li>
          </NavLink>
      )
    });
    return (
      <Fragment>
        Category:
        <ul>
          <NavLink tag={RouterNavLink} to={'/'}>
            <li>All products</li>
          </NavLink>
          {category}
        </ul>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  category: state.category.category
});

const mapDispatchToProps = dispatch => ({
  fetchCategory: () => dispatch(fetchCategory())
});

export default connect(mapStateToProps, mapDispatchToProps)(Category);